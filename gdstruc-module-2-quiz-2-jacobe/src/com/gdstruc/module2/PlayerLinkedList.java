package com.gdstruc.module2;

public class PlayerLinkedList
{
    private PlayerNode head;
    private int size;

    public int getSize()
    {
        return size;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null)
        {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        size++;

        if(playerNode.getNextPlayer() != null)
        {
            playerNode.getNextPlayer().setPreviousPlayer(playerNode);
        }
        head = playerNode;
    }

    public void removeFront()
    {
        if (head != null)
        {
            size--;
            var nodeToRemove = head;
            head = nodeToRemove.getNextPlayer();
            head.setPreviousPlayer(null);
        }
        else
        {
            System.out.println("There are no more nodes that can be removed.");
        }
    }

    public boolean contains(Player player)
    {
        PlayerNode current = head;

        while (current != null)
        {
            if(current.getPlayer() == player)
            {
                return true;
            }
            current = current.getNextPlayer();
        }
        return false;
    }

    public int indexOf(Player player)
    {
        int index = 0;
        PlayerNode current = head;

        while (current != null) {
            if (current.getPlayer() == player) {
                return index;
            }
            index++;
            current = current.getNextPlayer();
        }
        System.out.println("Player unknown.");
        return -1;
    }
}
