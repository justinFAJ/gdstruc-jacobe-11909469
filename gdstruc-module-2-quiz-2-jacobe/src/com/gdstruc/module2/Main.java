package com.gdstruc.module2;

public class Main {

    public static void main(String[] args)
    {
	    Player gordon = new Player(1, "Gordon", 900);
        Player adrian = new Player(2, "Adrian", 405);
        Player barney = new Player(3, "Barney", 600);
        Player eli = new Player(4, "Eli", 780);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(gordon);
        playerLinkedList.addToFront(adrian);
        playerLinkedList.addToFront(barney);
        playerLinkedList.addToFront(eli);

        playerLinkedList.printList();

        //Size and Index
        System.out.println("List Size: " + playerLinkedList.getSize());

        System.out.println("Gordon index: " + playerLinkedList.indexOf(gordon));
        System.out.println("Adrian index: " + playerLinkedList.indexOf(adrian));
        System.out.println("Barney index: " + playerLinkedList.indexOf(barney));
        System.out.println("Eli index: " + playerLinkedList.indexOf(eli));

        //Contains
        System.out.println("List containing Gordon: " + playerLinkedList.contains(gordon));

        //Front Node Removal
        System.out.println("\n\nRemoving node at the front.");

        playerLinkedList.removeFront();
        playerLinkedList.printList();

        System.out.println("List Size: " + playerLinkedList.getSize());

        System.out.println("Gordon index: " + playerLinkedList.indexOf(gordon));
        System.out.println("Adrian index: " + playerLinkedList.indexOf(adrian));
        System.out.println("Barney index: " + playerLinkedList.indexOf(barney));
        System.out.println("\nEli index: " + playerLinkedList.indexOf(eli));

        System.out.println("List containing Eli: " + playerLinkedList.contains(eli));
    }
}
