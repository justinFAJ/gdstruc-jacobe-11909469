package com.justinferdinand;

public class Main {

    public static void main(String[] args) {

        float[] cokeFloat = new float[20];

        cokeFloat[0] = 1.1f;
        cokeFloat[1] = 2.1f;
        cokeFloat[2] = 3.1f;
        cokeFloat[3] = 4.1f;
        cokeFloat[4] = 5.1f;
        cokeFloat[5] = 6.1f;
        cokeFloat[6] = 7.1f;
        cokeFloat[7] = 8.1f;
        cokeFloat[8] = 9.1f;
        cokeFloat[9] = 10.1f;
        cokeFloat[10] = 11.1f;
        cokeFloat[11] = 12.1f;
        cokeFloat[12] = 13.1f;
        cokeFloat[13] = 14.1f;
        cokeFloat[14] = 15.1f;
        cokeFloat[15] = 16.1f;
        cokeFloat[16] = 17.1f;
        cokeFloat[17] = 18.1f;
        cokeFloat[18] = 19.1f;
        cokeFloat[19] = 20.1f;

        System.out.println("20 floats");
        for (int i = 0; i < cokeFloat.length; i++)
        {
            System.out.println("Element Index: " + i + " " + cokeFloat[i]);
        }
    }
}
