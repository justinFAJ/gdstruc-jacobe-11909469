package com.gdstruc.module7;

public class Main
{
    public static void main(String[] args)
    {
        Tree tree = new Tree();

        tree.insert(69);
        tree.insert(27);
        tree.insert(23);
        tree.insert(16);
        tree.insert(8);
        tree.insert(3);

        tree.traverseInOrder();
        tree.traverseInOrderReverse();

        System.out.println("MIN: " + tree.getMin());
        System.out.println("MAX: " + tree.getMax());

    }
}
