package com.gdstruc.module4;

import java.util.Scanner;
import java.util.Random;

public class Main {

    public static boolean playRound(ArrayQueue queue)
    {
        Random rand = new Random();
        int playersToAdd = rand.nextInt(7) + 1;

        for (int i = 0; i < playersToAdd; i++)
        {
            Player playerToAdd = new Player(rand.nextInt(200), Player.playeruserNames[rand.nextInt(Player.playeruserNames.length)],
                    rand.nextInt(50));
            queue.add(playerToAdd);
            System.out.println(playerToAdd + " Started Queuing.");
        }

        System.out.println(System.lineSeparator());
        System.out.println("Players in queue: ");
        queue.printQueue();
        System.out.println(System.lineSeparator());

        if (queue.size() >= 5)
        {
            System.out.println("= Creating game =");
            for (int i = 0; i < 5; i++)
            {
                System.out.println(queue.remove() + " is joining the game.");
            }

            return true;
        }

        System.out.println("Not enough players." + " \r \n");

        return false;
    }

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        ArrayQueue queue = new ArrayQueue(25);

        int games = 0;
        int turn = 1;

        while (games < 10)
        {
            System.out.println("Games created: " + games);
            System.out.println("Turn No: " + turn);
            System.out.println(System.lineSeparator());

            if (playRound(queue))
            {
                games++;
            }

            turn++;

            System.out.println(System.lineSeparator());
            System.out.println("Press Enter to Continue. \r\n");
            scanner.nextLine();
        }
    }
}
