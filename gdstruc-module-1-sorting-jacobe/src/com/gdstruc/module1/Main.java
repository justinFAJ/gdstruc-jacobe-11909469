package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[10];

        numbers[0] = 3;
        numbers[1] = 4;
        numbers[2] = 7;
        numbers[3] = 8;
        numbers[4] = 6;
        numbers[5] = 1;
        numbers[6] = 10;
        numbers[7] = 2;
        numbers[8] = 9;
        numbers[9] = 5;

        System.out.println("Before sorting");
        printArrayElements(numbers);

        //Input the desired sorting algorithm here
        selectionSort(numbers);

        System.out.println("\n\nAfter sorting");
        printArrayElements(numbers);
    }

    //Made in descending order.
    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr [i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    //Made in descending order.
    private static void selectionSort(int [] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    private static void printArrayElements(int[] arr)
    {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }

}
