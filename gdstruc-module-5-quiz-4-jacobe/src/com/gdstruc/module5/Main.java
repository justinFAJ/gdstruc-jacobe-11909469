package com.gdstruc.module5;

public class Main {

    public static void main(String[] args)
    {
        Player s1mple = new Player(88, "S1mple", 99);
        Player shroud = new Player(69, "Shroud", 58);
        Player kennys = new Player(122, "KennyS", 61);
        Player niko = new Player(99, "Niko", 74);

        SimpleHashTable hashTable = new SimpleHashTable();

        hashTable.put(s1mple.getUserName(), s1mple);
        hashTable.put(shroud.getUserName(), shroud);
        hashTable.put(kennys.getUserName(), kennys);
        hashTable.put(niko.getUserName(), niko);

        hashTable.printHashTable();

        hashTable.remove("KennyS");
        hashTable.remove("Niko");

        hashTable.printHashTable();
    }
}
