package com.gdstruc.midterms.jacobe;
import java.util.Random;
import java.util.Scanner;

public class Main
{
    private static void roundStart(CardStack hand, CardStack deck, CardStack discardedPile)
    {
        Random rand = new Random();
        int randNum = rand.nextInt(5) + 1;
        int numPick = rand.nextInt(3);

        switch (numPick)
        {
            case 0 -> drawDeck(hand, deck, randNum);
            case 1 -> discard(hand, discardedPile, randNum);
            case 2 -> drawDiscardedPile(hand, discardedPile, randNum);
        }

        roundInfo(hand, deck, discardedPile);
        System.out.println("\n\n--------------------------------------------------------------");
    }

    private static void drawDeck(CardStack hand, CardStack deck, int count)
    {
        int count_ = count;
        System.out.println("\nDrawn " + count + " card/s from the deck.");

        if (count > deck.size())
        {
            count_ = deck.size();
            System.out.println("\nCan only draw: " + count_ + " card/s, due to the deck only having: " + count_ + " cards in the deck.");
        }

        for (int i = 0; i < count_; i++)
        {
            hand.push(deck.pop());
        }
    }

    private static void discard(CardStack hand, CardStack discardedPile, int count)
    {
        int count_ = count;
        System.out.println("\nDiscarded: " + count + " card/s from player hand.");

        if (count > hand.size())
        {
            count_ = hand.size();
            System.out.println("\nOnly discarded: " + count_ + " card/s from the player hand because there is/are only " + count_ + " cards in the player hand.");
        }

        for (int i = 0; i < count_; i++)
        {
            discardedPile.push(hand.pop());
        }
    }

    private static void drawDiscardedPile(CardStack hand, CardStack discardedPile, int count)
    {
        int count_ = count;
        System.out.println("\nDrawn " + count + " card/s from the discarded pile of cards.");

        if (count > discardedPile.size())
        {
            count_ = discardedPile.size();
            System.out.println("\nCan only draw :" + count_ + " card/s, because there is/are only: " +  count_ + " in the discarded pile.");
        }

        for (int i = 0; i < count_; i++)
        {
            hand.push(discardedPile.pop());
        }
    }

    private static void roundInfo(CardStack hand, CardStack deck, CardStack discardedPile)
    {
        System.out.print("Player hand: ");
        hand.printStack();
        System.out.println("\n\nPlayer hand size: " + hand.size());
        System.out.println("\nPlayer deck size: " + deck.size());
        System.out.println("\nDiscarded pile size: " + discardedPile.size());
    }

    public static void main(String[] args)
    {
        Random rand = new Random();

        String[] pokemonCards = {"Charizard", "Bulbasaur", "Pickachu", "Voltorb", "Geodude", "Gyarados", "Magikarp", "Blastoise", "Charmander", "Wingull"};

        CardStack deckPlayer = new CardStack();
        CardStack handPlayer = new CardStack();
        CardStack discardedPile = new CardStack();

        int deckStart = 30;

        for (int i = 0; i < deckStart; i++)
        {
            deckPlayer.push(new Card(pokemonCards[rand.nextInt(pokemonCards.length)]));
        }

        while (deckPlayer.size() > 0)
        {
            roundStart(handPlayer, deckPlayer, discardedPile);
        }
    }
}
